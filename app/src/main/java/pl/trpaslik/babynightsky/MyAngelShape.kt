/*
    This file is part of Baby Night Sky.

    Baby Night Sky is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Night Sky. If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky

import android.graphics.Path

object MyAngelShape {
    private const val WIDTH = 64f
    private const val HEIGHT = 64f

    val path: Path by lazy { buildPath() }

    private fun buildPath() = Path().apply {
        moveTo(32F, 1.8F);
        cubicTo(28.33F, 1.857F, 25.37F, 5.05F, 25.37F, 9F)
        cubicTo(25.3721F, 11.94F, 27.19F, 14.42F, 27.23F, 15.83F)
        cubicTo(25.61F, 15.8977F, 25.54F, 15.898F, 23.64F, 12.16F)
        cubicTo(22.976F, 11.211F, 21.15F, 8.53F, 16.91F, 8.53F)
        cubicTo(12.67F, 8.529342F, 9.24F, 12.35F, 9.24F, 17.07F)
        cubicTo(9.382F, 20.66F, 10.047F, 22.88F, 10.63F, 26.1F)
        cubicTo(10.6881F, 29.15F, 10.898F, 31.25F, 9.63F, 33.37F)
        cubicTo(8.362F, 35.49F, 7.73F, 38.03F, 9.204F, 38.98F)
        cubicTo(10.674F, 39.929F, 13.254F, 38.9544F, 14.954F, 36.8F)
        lineTo(20.944F, 30.18F)
        cubicTo(19.644F, 40.78F, 16.994F, 47.18F, 14.424F, 55.48F)
        cubicTo(14.423828F, 59.41F, 22.124F, 62.16F, 31.724F, 62.23F)
        cubicTo(31.723738F, 62.2312F, 31.724262F, 62.2327F, 31.724F, 62.2339F)
        cubicTo(31.913F, 62.2409F, 32.094F, 62.2339F, 32.275F, 62.2339F)
        cubicTo(32.274738F, 62.2327F, 32.275261F, 62.2312F, 32.275F, 62.23F)
        cubicTo(41.875F, 62.1593F, 49.575F, 59.4F, 49.575F, 55.48F)
        cubicTo(47.005F, 47.2F, 44.355F, 40.78F, 43.055F, 30.18F)
        lineTo(49.045F, 36.8F)
        cubicTo(50.755F, 38.95F, 53.325F, 39.92F, 54.795F, 38.98F)
        cubicTo(56.265F, 38.032F, 55.637F, 35.48F, 54.369F, 33.37F)
        cubicTo(53.099F, 31.26F, 53.309F, 29.15F, 53.369F, 26.1F)
        cubicTo(53.952F, 22.89F, 54.619F, 20.67F, 54.759F, 17.07F)
        cubicTo(54.759581F, 12.36F, 51.319F, 8.53F, 47.089F, 8.53F)
        cubicTo(42.849F, 8.530542F, 41.019F, 11.22F, 40.359F, 12.16F)
        cubicTo(38.459F, 15.89F, 38.389F, 15.89F, 36.769F, 15.83F)
        cubicTo(36.8078F, 14.42F, 38.629F, 11.93F, 38.629F, 9.0F)
        cubicTo(38.629066F, 5.06F, 35.669F, 1.86F, 31.999F, 1.8F)

        close()
        offset(-WIDTH / 2, -HEIGHT / 2)
    }

}
