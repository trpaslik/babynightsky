/*
    This file is part of Baby Night Sky.

    Baby Night Sky is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Night Sky. If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Paint.Style
import android.graphics.Path
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable

class MyShapeDrawable(private val origPath: Path, ttl: Int) : Drawable() {
    val strokePaint: Paint = Paint()
    val fillPaint: Paint = Paint()
    private val transPath: Path = Path() // transformed original path
    private val matrix: Matrix = Matrix()

    var scale = 1f
        set(scale) {
            mustTransform = true
            field = scale
            width = this.width
        }
    var rotate = 0f
        set(rotate) {
            var rotate = rotate
            if (rotate > 360) {
                rotate -= 360f
            } else if (rotate < -360) {
                rotate += 360f
            }
            mustTransform = true
            field = rotate
        }
    var x = 0f
        set(x) {
            mustTransform = true
            field = x
        }
    var y = 0f
        set(y) {
            mustTransform = true
            field = y
        }
    // strokePaint.setShadowLayer(scale * width * 0.75f, 0f, 0f,
    // Color.GRAY);
    private var width = 16f
        set(width) {
            field = width
            strokePaint.strokeWidth = this.scale * width
        }

    private var dRotate = 0f
    private var dScale = 0f

    private var mustTransform = true
    private val endTime: Long = System.currentTimeMillis() + ttl // when it's time to pass away...
    var style = Style.STROKE

    init {
        fillPaint.color = -0x330000ab
        fillPaint.style = Style.FILL
        // fillPaint.setAntiAlias(true);

        strokePaint.color = -0x3300aaab
        // strokePaint.setAntiAlias(true);
        strokePaint.style = Style.STROKE

        scale = 1f
        x = 0f
        y = 0f
        rotate = 0f
        width = 3f // default stroke width (scalable)
        alpha = 0xcc
    }

    internal fun timeToPassAway(): Boolean {
        return System.currentTimeMillis() > endTime
    }

    override fun draw(canvas: Canvas) {
        if (mustTransform) {
            transPath.set(origPath)
            matrix.setScale(this.scale, this.scale)
            matrix.postRotate(this.rotate)
            matrix.postTranslate(this.x, this.y)
            origPath.transform(matrix, transPath)
            mustTransform = false
        }
        if (style == Style.STROKE) {
            canvas.drawPath(transPath, strokePaint)
        }
        if (style == Style.FILL){
            canvas.drawPath(transPath, fillPaint)
        }
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    override fun getAlpha(): Int {
        return fillPaint.alpha
    }

    override fun setAlpha(alpha: Int) {
        fillPaint.alpha = alpha
        strokePaint.alpha = alpha
    }

    override fun setColorFilter(cf: ColorFilter?) {
        fillPaint.colorFilter = cf
        strokePaint.colorFilter = cf
    }

    fun setRotateDelta(dRotate: Float) {
        this.dRotate = dRotate
    }

    fun setScaleDelta(dScale: Float) {
        this.dScale = dScale
    }

    fun step() {
        if (dRotate != 0f) {
            rotate = this.rotate + dRotate
        }
        if (dScale != 0f) {
            scale = this.scale * dScale
        }
    }

}
