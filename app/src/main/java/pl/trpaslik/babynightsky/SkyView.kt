/*
    This file is part of Baby Night Sky.

    Baby Night Sky is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Night Sky. If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Path
import android.os.CountDownTimer
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import java.util.*


enum class PlayMode {
    MANUAL,
    AUTO_FIRE
}

@SuppressLint("ViewConstructor")
class SkyView(ctx: Context, val onShapeCreatedCallback: () -> Unit) : View(ctx) {

    private val rnd = Random()
    private val starDrawables = LinkedList<MyShapeDrawable>()

    private val displayMetrics = Resources.getSystem().displayMetrics
    private val scale = displayMetrics.density
    private val pxToDp = Matrix().apply {
        setScale(scale, scale)
    }

    private val starPath = MyStarShape.path.apply { transform(pxToDp) }
    private val moonPath = MyMoonShape.path.apply { transform(pxToDp) }
    private val angelPath = MyAngelShape.path.apply { transform(pxToDp) }

    private var lastX: Int = 0
    private var lastY: Int = 0
    private var mode = PlayMode.MANUAL

    init {
        setBackgroundColor(SKY_DARK_BLUE)
        object : CountDownTimer(5000, 32) {

            override fun onFinish() {
                this.start()
            }

            override fun onTick(millisUntilFinished: Long) {
                val iterator = starDrawables.iterator()
                while (iterator.hasNext()) {
                    val star = iterator.next()
                    if (star.timeToPassAway()) {
                        iterator.remove()
                        // Log.e("stars", "stars:" + starDrawables.size());
                    } else {
                        star.step()
                    }
                }
                invalidate()
            }

        }.start()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.pointerCount == 2 && event.action == MotionEvent.ACTION_POINTER_UP) {
            changePlayMode()
        }

        if (event.action == MotionEvent.ACTION_DOWN) {
            lastX = event.x.toInt()
            lastY = event.y.toInt()
            addShape(lastX, lastY, getScaleForTouch(), 3000 + rnd.nextInt(1000))
        } else if (event.action == MotionEvent.ACTION_MOVE) {
            val x = event.x.toInt()
            val y = event.y.toInt()
            val dxS = Math.abs((lastX - x) * (lastX - x))
            val dyS = Math.abs((lastY - y) * (lastY - y))
            val distLimit = TOLERANCE * scale
            if (dxS + dyS > distLimit * distLimit) {
                lastX = x
                lastY = y
                addShape(lastX, lastY, getScaleForDrag(),
                        2500 + rnd.nextInt(1000))
            }
        }
        return true
    }

    private fun changePlayMode() {
        mode = when (mode) {
            PlayMode.MANUAL -> PlayMode.AUTO_FIRE
            PlayMode.AUTO_FIRE -> PlayMode.MANUAL
        }
        autoFire()
    }

    private fun autoFire() {
        if (mode == PlayMode.AUTO_FIRE) {
            addShape(rnd.nextInt(1 + displayMetrics.widthPixels),
                    rnd.nextInt(1 + displayMetrics.heightPixels),
                    getScaleForTouch(),
                    3000 + rnd.nextInt(1000))
            Handler().postDelayed({
                autoFire()
            }, (100 + rnd.nextInt(300)).toLong())
        }
    }


    private fun getScaleForTouch(): Float {
        return 0.5f + rnd.nextFloat() * MAX_SCALE_TOUCH
    }

    private fun getScaleForDrag(): Float {
        return 1 + rnd.nextFloat() * MAX_SCALE_DRAG
    }

    private fun getPath(): Path {
        val luck = rnd.nextDouble()
        return when {
            (luck < 0.20) -> angelPath
            (luck < 0.30) -> moonPath
            else -> starPath
        }
    }

    private fun addShape(x: Int, y: Int, scale: Float, ttl: Int) {
        if (starDrawables.size > MAX_STARS) {
            starDrawables.removeAt(0) // remove any
        }
        val starDrawable = MyShapeDrawable(getPath(), ttl)
        val col = randomColor()
        starDrawable.fillPaint.color = col
        starDrawable.strokePaint.color = col
        starDrawable.x = x.toFloat()
        starDrawable.y = y.toFloat()
        starDrawable.scale = scale
        starDrawable.setScaleDelta(0.96f)
        starDrawable.rotate = (y / 4).toFloat()
        if (rnd.nextBoolean()) {
            starDrawable.style = Paint.Style.FILL
        } else {
            starDrawable.style = Paint.Style.STROKE
        }
        starDrawable.setRotateDelta(-6 + rnd.nextFloat() * 12)
        starDrawables.add(starDrawable)
        // Log.e("stars", "stars:" + starDrawables.size());
        onShapeCreatedCallback()
    }

    private fun randomColor(): Int {
        return Color.HSVToColor(0xCC, // a little bit of transparency
                floatArrayOf(rnd.nextInt(360).toFloat(), 0.9f, 0.8f))
    }

    override fun draw(canvas: android.graphics.Canvas) {
        super.draw(canvas)
        for (shape in starDrawables) {
            shape.draw(canvas)
        }
    }

    companion object {
        private const val MAX_SCALE_TOUCH = 6f
        private const val MAX_SCALE_DRAG = 3f
        private const val MAX_STARS = 32
        private const val SKY_DARK_BLUE = -0xddddab
        private const val TOLERANCE = 120
    }


}
