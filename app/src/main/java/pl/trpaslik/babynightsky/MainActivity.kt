/*
    This file is part of Baby Night Sky.

    Baby Night Sky is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Night Sky. If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky

import android.app.Activity
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.media.SoundPool
import android.view.Window
import java.util.*

class MainActivity : Activity() {

    private lateinit var sky: SkyView
    private var soundPool: SoundPool? = null
    private var soundId = 0
    private val rnd = Random()
    private val rates = floatArrayOf(1f, 0.9f, 1.1f, 0.8f, 1.2f)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)

        volumeControlStream = AudioManager.STREAM_MUSIC

        sky = SkyView(this) { playSound() }
        setContentView(sky)

        window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        sky.systemUiVisibility = fullScreen()
    }

    private val fillScreenPreKitKat =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
            View.SYSTEM_UI_FLAG_FULLSCREEN or
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

    private fun fullScreen(): Int =
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) fillScreenPreKitKat
            else View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or fillScreenPreKitKat

    private fun getSoundPool(): SoundPool {
        if (soundPool == null) {
            @Suppress("DEPRECATION")
            soundPool = SoundPool(Math.max(3, rates.size),
                    AudioManager.STREAM_MUSIC, 0)
        }
        return soundPool as SoundPool
    }

    override fun onResume() {
        super.onResume()
        soundId = getSoundPool().load(this, R.raw.ding, 1)
    }

    override fun onPause() {
        super.onPause()
        soundPool?.release()
        soundPool = null
    }

    private fun playSound() {
        if (soundId != 0) {
            getSoundPool().play(soundId, 1F, 1F, 0, 0,
                    rates[rnd.nextInt(rates.size)])
        }
    }

}
