/*
    This file is part of Baby Night Sky.

    Baby Night Sky is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Night Sky. If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky

import android.graphics.Path

object MyMoonShape {
    private const val WIDTH = 64f
    private const val HEIGHT = 64f

    val path: Path by lazy { buildPath() }

    private fun buildPath() = Path().apply {
            moveTo(41.3440f, 04.2188f)
            cubicTo(25.9920f, 04.2188f, 13.5320f, 16.6488f, 13.5320f, 31.9998f)
            cubicTo(13.5320f, 47.3518f, 25.9930f, 59.7808f, 41.3440f, 59.7808f)
            cubicTo(44.5516f, 59.7808f, 47.6022f, 59.2492f, 50.4690f, 58.2496f)
            cubicTo(41.7579f, 56.8469f, 38.8130f, 51.2518f, 34.3020f, 45.6466f)
            cubicTo(30.4038f, 45.9534f, 24.9471f, 41.6314f, 24.5088f, 40.6836f)
            cubicTo(28.4261f, 41.4727f, 33.8739f, 43.0375f, 33.4096f, 41.5105f)
            cubicTo(32.9453f, 39.9835f, 32.7923f, 36.3990f, 35.2915f, 36.5735f)
            cubicTo(43.1802f, 37.1241f, 37.1007f, 32.2899f, 35.9003f, 30.9445f)
            cubicTo(34.3253f, 29.1805f, 32.4793f, 27.4595f, 33.4083f, 22.1045f)
            cubicTo(35.3253f, 12.7855f, 42.9143f, 06.4925f, 50.4683f, 05.7496f)
            cubicTo(47.6013f, 04.7499f, 44.5503f, 04.2183f, 41.3433f, 4.2183f)
            close()
            moveTo(27.6443f, 17.4803f)
            cubicTo(30.3960f, 18.7964f, 28.1677f, 23.3968f, 25.5893f, 22.2797f)
            cubicTo(23.0110f, 21.1627f, 24.8926f, 16.1641f, 27.6443f, 17.4803f)
            close()
            offset(-WIDTH / 2, -HEIGHT / 2) // center
    }
}
