/*
    This file is part of Baby Night Sky.

    Baby Night Sky is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Night Sky. If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky

import android.graphics.Path

object MyStarShape {
    private const val WIDTH = 64f
    private const val HEIGHT = 64f

    val path: Path by lazy { buildPath() }

    private fun buildPath() = Path().apply {
            moveTo(16.884f, 49.008f);
            cubicTo(14.998f, 47.147f, 18.531f, 38.509f, 20.787f, 35.171f)
            cubicTo(18.044f, 33.077f, 10.161f, 27.869f, 11.601f, 24.111f)
            cubicTo(12.721f, 21.190f, 21.179f, 22.461f, 25.967f, 23.547f)
            cubicTo(27.571f, 19.336f, 30.201f, 11.040f, 33.647f, 11.393f)
            cubicTo(36.530f, 11.688f, 38.079f, 21.182f, 38.623f, 24.882f)
            cubicTo(42.152f, 25.427f, 52.027f, 25.068f, 52.556f, 28.430f)
            cubicTo(53.001f, 31.261f, 45.028f, 34.363f, 41.265f, 37.330f)
            cubicTo(41.575f, 42.113f, 45.043f, 50.137f, 42.196f, 51.677f)
            cubicTo(39.446f, 53.165f, 32.634f, 46.193f, 30.242f, 43.689f)
            cubicTo(25.949f, 46.737f, 19.170f, 51.263f, 16.885f, 49.009f)
            close()
            offset(-WIDTH / 2, -HEIGHT / 2)
    }

}
