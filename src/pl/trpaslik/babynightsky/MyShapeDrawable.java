/*
    This file is part of Baby Sky Night.

    Baby Sky Night is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Sky Night.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

public class MyShapeDrawable extends Drawable {
	private final Paint strokePaint;
	private final Paint fillPaint;
	private final Path origPath;
	private final Path transPath; // transformed original path
	private final Matrix matrix;

	private float scale = 1f;
	private float rotate = 0f;
	private float x = 0f;
	private float y = 0f;
	private float width = 16f;

	private float dRotate = 0f;
	private float dScale = 0f;

	private boolean mustTransform = true;
	private final long endTime; // when it's time to pass away...

	public MyShapeDrawable(Path path, int ttl) {
		this.origPath = path;
		this.endTime = System.currentTimeMillis() + ttl;
		this.transPath = new Path();
		this.matrix = new Matrix();
		fillPaint = new Paint();
		fillPaint.setColor(0xccffff55);
		fillPaint.setStyle(Style.FILL);
		// fillPaint.setAntiAlias(true);

		strokePaint = new Paint();
		strokePaint.setColor(0xccff5555);
		// strokePaint.setAntiAlias(true);
		strokePaint.setStyle(Style.STROKE);

		setScale(1f);
		setX(0f);
		setY(0f);
		setRotate(0f);
		setWidth(2.5f); // default stroke width (scalable)
		setAlpha(0xcc);
	}

	boolean timeToPassAway() {
		return System.currentTimeMillis() > endTime;
	}

	@Override
	public void draw(Canvas canvas) {
		if (mustTransform) {
			transPath.set(origPath);
			matrix.setScale(scale, scale);
			matrix.postRotate(rotate);
			matrix.postTranslate(x, y);
			origPath.transform(matrix, transPath);
			mustTransform = false;
		}
		canvas.drawPath(transPath, strokePaint);
		canvas.drawPath(transPath, fillPaint);
	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSLUCENT;
	}

	public int getAlpha() {
		return fillPaint.getAlpha();
	}

	public Paint getFillPaint() {
		return fillPaint;
	}

	public Paint getStrokePaint() {
		return strokePaint;
	}

	@Override
	public void setAlpha(int alpha) {
		fillPaint.setAlpha(alpha);
		strokePaint.setAlpha(alpha);
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
		fillPaint.setColorFilter(cf);
		strokePaint.setColorFilter(cf);
	}

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		mustTransform = true;
		this.scale = scale;
		setWidth(width);
	}

	public float getRotate() {
		return rotate;
	}

	public void setRotate(float rotate) {
		if (rotate > 360) {
			rotate -= 360;
		} else if (rotate < -360) {
			rotate += 360;
		}
		mustTransform = true;
		this.rotate = rotate;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		mustTransform = true;
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		mustTransform = true;
		this.y = y;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
		// strokePaint.setShadowLayer(scale * width * 0.75f, 0f, 0f,
		// Color.GRAY);
		strokePaint.setStrokeWidth(scale * width);
	}

	public float getdRotate() {
		return dRotate;
	}

	public void setdRotate(float dRotate) {
		this.dRotate = dRotate;
	}

	public void setdScale(float dScale) {
		this.dScale = dScale;
	}

	public void step() {
		if (dRotate != 0) {
			setRotate(rotate + dRotate);
		}
		if (dScale != 0) {
			setScale(scale * dScale);
		}
	}

}
