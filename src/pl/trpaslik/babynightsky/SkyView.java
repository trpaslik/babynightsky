/*
    This file is part of Baby Sky Night.

    Baby Sky Night is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Sky Night.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Path;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class SkyView extends View {

	private static final float MAX_SCALE_TOUCH = 6f;
	private static final float MAX_SCALE_DRAG = 3f;
	// // TODO: decrease MAX_STARS dynamically if lags detected
	private static final int MAX_STARS = 32;
	private static final int SKY_DARK_BLUE = 0xff222255;
	private static final int TOLERANCE = 120;
	private Random rnd = new Random();
	private List<MyShapeDrawable> starDrawables;
	private int lastX;
	private int lastY;
	private WindowManager wm;
	private Path starPath;;
	private Path moonPath; // TODO: enums
	private float screenScale = 1f;

	public SkyView(Context context) {
		super(context);
		wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		setBackgroundColor(SKY_DARK_BLUE);
		starDrawables = new LinkedList<MyShapeDrawable>();
		new CountDownTimer(5000, 40) {

			@Override
			public void onFinish() {
				this.start();
			}

			@Override
			public void onTick(long millisUntilFinished) {
				Iterator<MyShapeDrawable> it = starDrawables.iterator();
				while (it.hasNext()) {
					MyShapeDrawable star = it.next();
					if (star.timeToPassAway()) {
						it.remove();
						// Log.e("stars", "stars:" + starDrawables.size());
					} else {
						star.step();
					}
				}
				invalidate();
			}

		}.start();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			lastX = (int) event.getX();
			lastY = (int) event.getY();
			addShape(lastX, lastY, getScaleForTouch(), 3000 + rnd.nextInt(1000));
		} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			int x = (int) event.getX();
			int y = (int) event.getY();
			int dxS = Math.abs((lastX - x) * (lastX - x));
			int dyS = Math.abs((lastY - y) * (lastY - y));
			float distLimit = (TOLERANCE * screenScale);
			if (dxS + dyS > distLimit * distLimit) {
				lastX = x;
				lastY = y;
				addShape(lastX, lastY, getScaleForDrag(event),
						2500 + rnd.nextInt(1000));
			}
		}
		return true;
	}

	private float getScaleForTouch() {
		return 0.5f + rnd.nextFloat() * MAX_SCALE_TOUCH;
	}

	private float getScaleForDrag(MotionEvent event) {
		return 1 + rnd.nextFloat() * MAX_SCALE_DRAG;
	}

	private float getScreenScaleSizeFactor(float width, float height) {
		int referenceScreenSize = 720;
		Display dspl = wm.getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		dspl.getMetrics(displayMetrics);
		int screenSize = Math.min(displayMetrics.widthPixels,
				displayMetrics.heightPixels);
		float shapeSize = Math.max(width, height);
		float scaledShapeSize = (shapeSize * screenSize) / referenceScreenSize;
		return scaledShapeSize / shapeSize;
	}

	private Path getPath() {
		if (starPath == null) {
			starPath = MyStarShape.getPath();
			Matrix m = new Matrix();
			screenScale = getScreenScaleSizeFactor(MyStarShape.WIDTH,
					MyStarShape.HEIGHT);
			m.setScale(screenScale, screenScale);
			starPath.transform(m);
		}
		if (moonPath == null) {
			moonPath = MyMoonShape.getPath();
			Matrix m = new Matrix();
			screenScale = getScreenScaleSizeFactor(MyMoonShape.WIDTH,
					MyMoonShape.HEIGHT);
			m.setScale(screenScale * 1.5f, screenScale * 1.5f);
			moonPath.transform(m);
		}
		double luck = rnd.nextDouble();
		if (luck < 0.20) { // 20% chanse to get moon
			return moonPath;
		} else {
			return starPath;
		}
	}

	private void addShape(int x, int y, float scale, int ttl) {
		if (starDrawables.size() > MAX_STARS) {
			starDrawables.remove(0); // remove any
		}
		final MyShapeDrawable starDrawable = new MyShapeDrawable(getPath(), ttl);
		int col = randomColor();
		starDrawable.getFillPaint().setColor(col);
		starDrawable.getStrokePaint().setColor(col);
		starDrawable.setX(x);
		starDrawable.setY(y);
		starDrawable.setScale(scale);
		starDrawable.setdScale(0.96f);
		starDrawable.setRotate(y / 4);
		starDrawable.setdRotate(-6 + rnd.nextFloat() * 12);
		starDrawables.add(starDrawable);
		// Log.e("stars", "stars:" + starDrawables.size());
		MainActivity.playSound();
	}

	private int randomColor() {
		return Color.HSVToColor(0xCC, // a little bit of transparency
				new float[] { rnd.nextInt(360), 0.9f, 0.8f });
	}

	@Override
	public void draw(android.graphics.Canvas canvas) {
		super.draw(canvas);
		for (MyShapeDrawable shape : starDrawables) {
			shape.draw(canvas);
		}
	}

}
