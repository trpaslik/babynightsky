/*
    This file is part of Baby Sky Night.

    Baby Sky Night is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Sky Night.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky;

import android.graphics.Path;

public class MyStarShape {
	static private Path path;
	public static final float WIDTH = 64f;
	public static final float HEIGHT = 64f;

	// data/mystar.svg (created with inkscape)
	// scour -i mystar_orig.svg -o mystar2.svg --enable-id-stripping \
	// --enable-comment-stripping --shorten-ids --remove-metadata
	// http://www.professorcloud.com/svg-to-canvas/
	// bezierCurveTo() -> Path.cubicTo()
	// moveTo() -> Path.moveTo()
	// closePath() -> Path.close()
	private static Path buildPath() {
		path = new Path();

		path.moveTo(16.884f, 49.008f);
		path.cubicTo(14.998f, 47.147f, 18.531f, 38.509f, 20.787f, 35.171f);
		path.cubicTo(18.044f, 33.077f, 10.161f, 27.869f, 11.601f, 24.111f);
		path.cubicTo(12.721f, 21.190f, 21.179f, 22.461f, 25.967f, 23.547f);
		path.cubicTo(27.571f, 19.336f, 30.201f, 11.040f, 33.647f, 11.393f);
		path.cubicTo(36.530f, 11.688f, 38.079f, 21.182f, 38.623f, 24.882f);
		path.cubicTo(42.152f, 25.427f, 52.027f, 25.068f, 52.556f, 28.430f);
		path.cubicTo(53.001f, 31.261f, 45.028f, 34.363f, 41.265f, 37.330f);
		path.cubicTo(41.575f, 42.113f, 45.043f, 50.137f, 42.196f, 51.677f);
		path.cubicTo(39.446f, 53.165f, 32.634f, 46.193f, 30.242f, 43.689f);
		path.cubicTo(25.949f, 46.737f, 19.170f, 51.263f, 16.885f, 49.009f);
		path.close();
		path.offset(-WIDTH / 2, -HEIGHT / 2); // center

		return path;
	}

	public static Path getPath() {
		if (path == null) {
			path = buildPath();
		}
		return path;
	}

}
