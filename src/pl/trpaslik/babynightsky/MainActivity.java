/*
    This file is part of Baby Sky Night.

    Baby Sky Night is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Baby Sky Night is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Baby Sky Night.  If not, see <http://www.gnu.org/licenses/>.

 */

package pl.trpaslik.babynightsky;

import java.util.Random;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

	private SkyView sky;
	private static SoundPool soundPool = null;
	private static int soundId = 0;
	private static Random rnd = new Random();
	private static float[] rates = new float[] { 1f, 0.9f, 1.1f, 0.8f, 1.2f };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// hide application title
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// prevent screen dim
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// set full screen
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		sky = new SkyView(this);
		setContentView(sky);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)
				|| (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)) {
			return super.onKeyUp(keyCode, event);
		}
		return true;
	}

	private static void ensureSoundPool() {
		if (soundPool == null) {
			soundPool = new SoundPool(Math.max(3, rates.length),
					AudioManager.STREAM_MUSIC, 0);
		}
	}

	protected void onResume() {
		super.onResume();
		ensureSoundPool();
		soundId = soundPool.load(this, R.raw.ding, 1);
	}

	protected void onPause() {
		super.onPause();
		if (soundPool != null) {
			soundPool.release();
			soundPool = null;
		}
	}

	static public void playSound() {
		ensureSoundPool();
		if (soundId != 0) {
			soundPool.play(soundId, 1, 1, 0, 0,
					rates[rnd.nextInt(rates.length)]);
		}
	}

}
